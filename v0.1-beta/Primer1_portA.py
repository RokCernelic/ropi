#!/usr/bin/env python
# UL PeF, Rok Cernelic, 2014
from RoPi_lib import *


# ===========================================================================
# PORTA (ANALOG INPUT)
# ===========================================================================
'''
PORTA(pin)                      # 0-7 pins on PORTA are analog input pins
'''

# Example 1
while True:
    #print "%s %s %s %s" % (PORTA(0), PORTA(1), PORTA(2), PORTA(3))
    print "%04d %04d %04d %04d" % (PORTA(0), PORTA(1), PORTA(2), PORTA(3))