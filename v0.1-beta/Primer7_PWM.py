#!/usr/bin/env python
# UL PeF, Rok Cernelic, 2014
from RoPi_lib import *


# ===========================================================================
# PORTE (SERVO PWM DRIVER)
# ===========================================================================
'''
PORTE.pwm(pin, DC)                # Pin 0-15, DC = DutyCycle, must be integer
                                  # Frequency set to 50Hz, modify in library


# DCmin is pre-set in RoPi_lib
# DCmax is pre-set in RoPi_lib
'''

# Example 7
'''
# examples of manual servo position input
DC = DCmin + ((DCmax - DCmin) * (ADC / 1023.0))       # servo position with potentiometer via 10bit ADC input (PORTA)
DC = DCmin + ((DCmax - DCmin) * (percent / 100.0))      # percent 1 - 100
DC = DCmin + ((DCmax - DCmin) * (input / rangeMax))   # input is a number in rangeMax, could use servo angle
'''

while True:
    ADC = PORTA(0)
    DC = DCmin + ((DCmax - DCmin) * (ADC / 1023.0))
    PORTE.pwm(8, int(DC))
