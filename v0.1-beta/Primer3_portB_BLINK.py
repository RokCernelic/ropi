#!/usr/bin/env python
# UL PeF, Rok Cernelic, 2014
from RoPi_lib import *


# ===========================================================================
# PORTB (DIGITAL INPUT, OUTPUT)
# ===========================================================================
'''
PORTB.setup(pin, PORTB.IN)      # Ignore. Allerady set in the library for pins 0-7
PORTB.pullup(pin, 1)            # Ignore. Allerady set in the library for pins 0-7
PORTB.input(pin)                # Call input pin 0-7

PORTB.setup(pin, PORTB.OUT)     # Set pin as output (overrides library setup)
PORTB.output(pin, value)        # Set output pin 0-7 as High/Low
'''

# Example 3:
# Blink pin 7
PORTB.setup(7, PORTB.OUT)

while True:
    PORTB.output(7, 1)
    sleep(1)
    PORTB.output(7, 0)
    sleep(1)
