#!/usr/bin/env python
# UL PeF, Rok Cernelic, 2014
from RoPi_lib import *


# ===========================================================================
# LCD as GPIO on MCP23008
# ===========================================================================
'''LCD functions
LCD.begin(self, cols, lines)
LCD.home(self)              # set cursor position to zero
LCD.clear(self)             # command to clear display
LCD.setCursor(self, col, row) # Set kursos position
LCD.noDisplay(self)         # Turn the display off (quickly)
LCD.display(self)           # Turn the display on (quickly)
LCD.noCursor(self)          # Turns the underline cursor off
LCD.cursor(self)            # Turns the underline cursor on
LCD.noBlink(self)           # Turn the blinking cursor off
LCD.blink(self)             # Turn the blinking cursor on
LCD.DisplayLeft(self)       # These commands scroll the display without changing the RAM
LCD.scrollDisplayRight(self)# These commands scroll the display without changing the RAM
LCD.leftToRight(self)       # This is for text that flows Left to Right
LCD.rightToLeft(self)       # This is for text that flows Right to Left
LCD.autoscroll(self)        # This will 'right justify' text from the cursor
LCD.noAutoscroll(self)      # This will 'left justify' text from the cursor'''

# Example 8
# Print to two line (max 16 char)
message = raw_input("Vnos: ")
while 1:
    LCD.clear()
    LCD.message("%s\n%s" % ((message[0:8]), (message[8:16])))
    sleep(2)
    