#!/usr/bin/env python
# UL PeF, Rok Cernelic, 2014
from RoPi_lib import *


# ===========================================================================
# PORTE (SERVO PWM DRIVER)
# ===========================================================================
'''
PORTE.pwm(pin, DC)                # Pin 0-15, DC = DutyCycle, must be integer
                                  # Frequency set to 50Hz, modify in library


# DCmin is pre-set in RoPi_lib
# DCmax is pre-set in RoPi_lib
'''

# Example 6
while True:
    # PORTE.pwm(channel, position-DC)
    PORTE.pwm(8, DCmin)
    sleep(2)
    PORTE.pwm(8, DCmax)
    sleep(2)
    