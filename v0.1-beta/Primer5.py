#!/usr/bin/env python
# UL PeF, Rok Cernelic, 2014
from RoPi_lib import *


# ===========================================================================
# PORTC (DIGITAL OUTPUT, MOTOR DRIVER, MAX. 0.6A)
# ===========================================================================
'''
PORTC.output(pin, value)        # Set output pin 0-7 as High/Low
PORTE.pwm(0, DC)                # Set PWM to motor1 - PORTC(0-1), DC 0-4095
PORTE.pwm(1, DC)                # Set PWM to motor2 - PORTC(2-3), DC 0-4095 
PORTE.pwm(2, DC)                # Set PWM to motor3 - PORTC(4-5), DC 0-4095 
PORTE.pwm(3, DC)                # Set PWM to motor4 - PORTC(6-7), DC 0-4095
'''

# Example 5 
# Cycle motor1 speed from 0 to 4095 an back
PORTC.output(0, 0)
PORTC.output(1, 1)
def speed():
        for DC in range(0, 4095, 10):
            PORTE.pwm(2, DC)
            sleep(0.01)
        for DC in range(4095, -1, -10):
            PORTE.pwm(2, DC)
            sleep(0.01)

try:
    while True:
        speed()
except KeyboardInterrupt:
    # stop
    PORTC.output(0, 0)
    PORTC.output(1, 0)
    